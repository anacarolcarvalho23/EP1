# Orientação a Objetos 1/2016

## Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
	** abra a pasta EP1;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**

Para executar o programa:

* As imagens a serem copiadas devem estar na pasta "Doc";

* Quando o programa pedir o nome da imagem, o usuário deve colocar sem a extensão ".ppm";

* Após isso, o usuário deve selecionar a opção do filtro que ele deseja aplicar. 

* O usuário deve informar se ele deseja o filtro 1, 2, 3 ou 4.

* A imagem será salva na pasta "Doc".