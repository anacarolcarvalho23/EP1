#ifndef FILTRO_HPP
#define FILTRO_HPP
#include "Imagem.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class Filtro : public Imagem{

public:
	virtual void SalvarArq(Imagem *ref_img, Pxl *ref_pxl);

};

#endif