#ifndef NEGATIVO_HPP
#define NEGATIVO_HPP
#include "Filtro.hpp"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Filtro_NEG : public Filtro{
private:
	
public:
	void SalvarArq(Imagem *ref_img, Pxl *ref_pxl);
        Pxl* img_filtro(Imagem *ref_img, Pxl *ref_pxl, Pxl *ref_novaMatriz);
}; 

#endif