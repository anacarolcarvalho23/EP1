#ifndef PRETOBRANCO_HPP
#define PRETOBRANCO_HPP
#include "Filtro.hpp"
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

class Filtro_PB : public Filtro{
private:

public:
        Pxl* img_filtro(Imagem *ref_img, Pxl *ref_pxl, Pxl *ref_novaMatriz);
        void SalvarArq(Imagem *ref_img, Pxl *ref_pxl);

};


#endif

