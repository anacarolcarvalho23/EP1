#ifndef POLARIZADO_HPP
#define POLARIZADO_HPP
#include "Filtro.hpp"
#include <string>
#include <fstream>
#include <iostream>

using namespace std;

class Filtro_POL : public Filtro{
private: 

public:
	void SalvarArq(Imagem *ref_img, Pxl *ref_pxl);
        Pxl* img_filtro(Imagem *ref_img, Pxl *ref_pxl, Pxl *ref_novaMatriz);

};

#endif
