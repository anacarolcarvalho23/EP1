#ifndef Imagem_HPP
#define Imagem_HPP
#include <string>
#define Num 1020

using namespace std;

typedef struct Pxl{
	unsigned char red[Num][Num], green[Num][Num], blue[Num][Num];
}

Pxl;
class Imagem{

private:
	string file_img;
	string tipo_img;
	int largura, altura, intensidade;

public:
	Imagem();

	int getAltura();
	void setAltura(int altura);
	int getLargura();
	void setLargura(int largura);
	string  getTipo();
	void setTipo(string tipo_img);
	void setfile_img(string file_img);
	int getcolor_max();
	void setcolor_max(int intensidade);
	string getfile_img();
	
	Pxl* imgInfo(string file_img);
};

#endif