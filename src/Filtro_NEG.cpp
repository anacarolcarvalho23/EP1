#include <iostream>
#include "Filtro_NEG.hpp"
#include <string>
#include <fstream>
#include <stdlib.h>
#define Num 1020

using namespace std;

Pxl* Filtro_NEG:: img_filtro(Imagem *ref_img, Pxl *ref_pxl, Pxl *ref_novaMatriz){

	int intensidade, largura, altura, red[Num][Num], blue[Num][Num], green[Num][Num];

	largura = ref_img -> getLargura();
    intensidade = ref_img -> getcolor_max();
	altura = ref_img -> getAltura();

	for(int i = 0; i < largura; ++i){
        for(int j = 0; j < altura; ++j){

		red[i][j] = (int) ref_pxl->red[i][j];
		green[i][j] = (int)ref_pxl->green[i][j];
		blue[i][j] = (int)ref_pxl->blue[i][j];
		        
                ref_novaMatriz-> red[i][j] = intensidade - red[i][j];
                ref_novaMatriz-> green[i][j] = intensidade - green[i][j];
                ref_novaMatriz-> blue[i][j] = intensidade - blue[i][j];
		}
          }
return ref_novaMatriz;
}

void Filtro_NEG:: SalvarArq(Imagem *ref_img, Pxl *ref_pxl){

        string info;
        ofstream ref_copia;

	           info = ref_img->getfile_img();

        string novoNome = "./doc/negativo.ppm";
        ref_copia.open(novoNome.c_str());

            ref_copia << ref_img -> getTipo() << endl;
            ref_copia << ref_img -> getLargura() << endl;
            ref_copia << ref_img -> getAltura() << endl;
            ref_copia << ref_img -> getcolor_max() << endl;

        for(int i=0; i < ref_img->getLargura(); i++){
          for(int j=0; j < ref_img->getAltura(); j++){

                ref_copia.write((char *)(&ref_pxl->red[i][j]),1);
                ref_copia.write((char *)(&ref_pxl->green[i][j]),1);
                ref_copia.write((char *)(&ref_pxl->blue[i][j]),1);

                }
        }

        cout <<"Gravação concluída, filtro aplicado corretamente" << endl;
        ref_copia.close();


}
