#include "Filtro_PB.hpp"
#include <iostream>
#include <stdlib.h>
#include <string>
#include <fstream>
#define Num 1020

using namespace std;

Pxl* Filtro_PB:: img_filtro(Imagem *ref_img, Pxl *ref_pxl, Pxl *ref_novaMatriz){

    int red[Num][Num], blue[Num][Num], green[Num][Num], grayscale_value;
	
    int largura = ref_img -> getLargura();
	int altura = ref_img ->getAltura();
	int intensidade = ref_img->getcolor_max();

        for(int i = 0; i < largura; ++i){
           for(int j = 0; j < altura; ++j){

		red[i][j] = (int) ref_pxl->red[i][j];
		green[i][j] = (int) ref_pxl->green[i][j];
		blue[i][j] = (int) ref_pxl->blue[i][j];

		grayscale_value = (0.299 * red[i][j]) + (0.587 * green[i][j]) + (0.144 * blue[i][j]);
		
        if(grayscale_value > intensidade){
		  grayscale_value = intensidade;
		}
                ref_novaMatriz->red[i][j] = grayscale_value;
                ref_novaMatriz->green[i][j] = grayscale_value;
                ref_novaMatriz->blue[i][j] = grayscale_value;

                }
          }

return ref_novaMatriz;
}

void Filtro_PB:: SalvarArq(Imagem *ref_img, Pxl *ref_pxl){

        string info;
        ofstream ref_copia;

        info = ref_img->getfile_img();
        string novoNome = "./doc/pretobranco.ppm";
        ref_copia.open(novoNome.c_str());

        ref_copia << ref_img -> getAltura() << endl;
        ref_copia << ref_img -> getcolor_max() << endl;
        ref_copia << ref_img -> getTipo() << endl;
        ref_copia << ref_img -> getLargura() << endl;

        for(int i=0; i < ref_img->getLargura(); i++){
          for(int j=0; j < ref_img->getAltura(); j++){

                ref_copia.write((char *)(&ref_pxl->blue[i][j]),1);
                ref_copia.write((char *)(&ref_pxl->green[i][j]),1);
                ref_copia.write((char *)(&ref_pxl->red[i][j]),1);

                }
        }
        ref_copia.close();
        cout <<"Gravação concluída, filtro aplicado corretamente" << endl;

}

