#include "Imagem.hpp"
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#define Num 1020

using namespace std;

Imagem::Imagem(){

    setAltura(0);
    setLargura(0);
    setTipo(tipo_img);
    setcolor_max(0);

}
int Imagem::getAltura(){
    return altura;
}
void Imagem::setAltura(int altura){
    this -> altura = altura;
}
int Imagem::getLargura(){
    return largura;
}
void Imagem::setLargura(int largura){
    this -> largura = largura;
}
string Imagem::getTipo(){
    return tipo_img;
}
void Imagem::setTipo(string tipo_img){
    this -> tipo_img = tipo_img;
}
void Imagem::setcolor_max(int intensidade){
    this -> intensidade = intensidade;
}
int Imagem::getcolor_max(){
    return intensidade;
}
void Imagem::setfile_img(string file_img){
        this -> file_img = file_img;
}
string Imagem::getfile_img(){
        return file_img;
}
   
    Pxl* Imagem::imgInfo(string file_img){
            ifstream ref_file;
            string arquivo = "./doc/" + file_img+".ppm";
            ref_file.open(arquivo.c_str(), ios_base:: binary);

            if(!ref_file.is_open()){

                cout << "Erro ao abrir o arquivo." << endl;

                    ref_file.close();
            exit(1);
              }

            else{
             }

            string tipo_img, comentario, linha;
            int largura = 0, altura = 0, intensidade = 0;
            char r,g,b,c;
            
            std::streampos aux;
            Pxl *ref_pxl = new Pxl;

            getline(ref_file,tipo_img);

            if(tipo_img == "P6"){
            }

            else{ 
                cout << "O arquivo não está no formato .ppm." << endl;
                    ref_file.close();
                    exit(1);
                    }

            do{
            aux = ref_file.tellg();
            ref_file >> c;

            if(c != '#'){
                ref_file.seekg(aux);
              }
            else{
                getline(ref_file,comentario);    
                 }

          } while(c == '#');


        ref_file >> largura;
        ref_file >> altura;
        ref_file >> intensidade;

        getline(ref_file,linha);
        ref_file.seekg(0,ios_base::cur);

        setAltura(altura);
        setLargura(largura);
        setTipo(tipo_img);
        setcolor_max(intensidade);

        cout << "Altura:" << getAltura() <<endl;
        cout << "Largura:" << getLargura() <<endl;

              for( int i=0; i < largura; ++i){
               for(int j=0; j < altura; ++j){
                    ref_file.get(r);
                    ref_pxl->red[i][j] = (unsigned char) r;
                    ref_file.get(g);
                        ref_pxl->green[i][j] = (unsigned char)g;
                    ref_file.get(b);
                    ref_pxl->blue[i][j] = (unsigned char)b;
               }
             }


        ref_file.close();

        return ref_pxl;

    }

