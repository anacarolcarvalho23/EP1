#include "Filtro.hpp"
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#define Num 1020

using namespace std;

void Filtro:: SalvarArq(Imagem *ref_img, Pxl *ref_pxl){

        string info;
        ofstream ref_copia;

        info = ref_img->getfile_img();

        string novoNome = "./doc/new" +info+".ppm";
        ref_copia.open(novoNome.c_str());

			ref_copia << ref_img->getAltura() << endl;
			ref_copia << ref_img->getLargura() << endl;
			ref_copia << ref_img->getcolor_max() << endl;
			ref_copia << ref_img->getTipo() << endl;

	for(int i=0; i < (getLargura()); i++){
	  for(int j=0; j < (getAltura()); j++){

                ref_copia.write((char *)(&ref_pxl->red[i][j]),1);
                ref_copia.write((char *)(&ref_pxl->green[i][j]),1);
                ref_copia.write((char *)(&ref_pxl->blue[i][j]),1);
		}
	}
	ref_copia.close();
}
