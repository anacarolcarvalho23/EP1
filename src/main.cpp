#include <iostream>
#include <fstream>
#include "Imagem.hpp"
#include "Filtro_PB.hpp"
#include "Filtro_NEG.hpp"
#include "Filtro_POL.hpp"
#include <cstdio>
#include <string>

using namespace std;

int main ()
{
	int opcao;
	string foto;
	string caminho;
	Imagem figura,*ref_img;
	Filtro filtro;
	Filtro_PB pb;
	Filtro_POL pol;
	Filtro_NEG neg;
	
	 cout << "Digite a imagem que você deseja aplicar o filtro: ";
	 cin >> foto;
	figura.setfile_img(foto);

	Pxl *ref_pxl = new Pxl;
	Pxl *ref_novaMatriz = new Pxl;
	ref_pxl = figura.imgInfo(foto);
	
	ref_img = &figura;

cout << "Escolha o filtro:" << endl;
	cout << "(1) Preto e Branco" << endl;
	cout << "(2) Polarizado" << endl;
	cout << "(3) Negativo" << endl;
	cout << "(4) Média" << endl;

	cin >> opcao;

	switch(opcao){
		case 1:
			ref_novaMatriz = pb.img_filtro(ref_img,ref_pxl,ref_novaMatriz);
			pb.SalvarArq(ref_img,ref_novaMatriz);	
				break;

		case 2:
			ref_novaMatriz = pol.img_filtro(ref_img,ref_pxl,ref_novaMatriz);
			pol.SalvarArq(ref_img,ref_novaMatriz);		
				break;

		case 3:
			ref_novaMatriz = neg.img_filtro(ref_img,ref_pxl,ref_novaMatriz);
			neg.SalvarArq(ref_img,ref_novaMatriz);
				break;

		case 4:
			cout << "Função ainda não implementada" << endl;
				break;

	}


	return 0;
}
